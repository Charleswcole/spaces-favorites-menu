AJS.$(document).ready(function () {
	var $spaceslink = AJS.$("#space-directory-link");
	
	if (AJS.params.remoteUser && $spaceslink.length > 0) {
		AJS.$.ajax({
			url: AJS.params.contextPath + '/rest/spacedirectory/1/search',
			type: 'GET',
			data: ({label: ['~'+ AJS.params.remoteUser +':favourite', '~'+ AJS.params.remoteUser +':favorite'] }),
			dataType: "json",
	  		success: function(data){
	  			if (data.totalSize > 0) {
					$spaceslink.addClass("aui-dropdown2-trigger");
					$spaceslink.attr("aria-owns", "favorite-spaces-menu");
					$spaceslink.attr("aria-haspopup", true);
					$dropdown = SK.confluence.spacesdropdown.dropdown({
						id: "favorite-spaces-menu",
						spaces: data.spaces,
						ajsparams: AJS.params
					});
					$spaceslink.after($dropdown);	
				}  			
	  		}
		});
	}

});